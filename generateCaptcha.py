import random

from captcha import Captcha

__alphabet = 'ABCDEFGHIJKLMNPQRSTUVWXYZ123456789'


def randomString(length: int = 6) -> str:
    return ''.join(random.choices(__alphabet, k=length))


def test():
    # Initialize Captcha object with "Text" as text and FreeMono as font
    c = Captcha(randomString, "fonts/Roboto/Roboto-Light.ttf")

    # Get PIL Image object
    text, image = c.image

    print(text)  # 'Text'
    print(type(image))  # <class 'PIL.Image.Image'>

    # Get BytesIO object (note that it will represent a different image, just
    # with the same text)
    text, bytes = c.bytes

    print(text)  # 'Text'
    print(type(bytes))  # <class '_io.BytesIO'>

    # Save a PNG file 'test.png'
    text, file = c.write('test.png')

    print(text)  # 'Text'
    print(file)  # 'test.png'


if __name__ == '__main__':
    test()
