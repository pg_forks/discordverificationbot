# -*- coding: utf-8 -*-

"""This module defines Captcha and CaptchaError classes."""

import os
import random
from functools import wraps
from io import BytesIO

from PIL import Image, ImageDraw, ImageFont


class CaptchaError(Exception):
    """Exception class for Captcha errors."""


class Captcha:
    r"""
    Captcha class.

    Captcha can be use to create PIL Images, BytesIO objects and image
    files with CAPTCHA messages. User has to provide at least a source
    (a string containing text used in CAPTCHA image or a callable object
    returning a string) and a filepath to TTF font file.

    Additionally, Captcha allows to define image size and estimated
    margins, used in automatically calculating font size. By default,
    Captcha generates a PNG image using bicubic resampling filter
    (configurable).

    Optionally, user can define white noise, making it less readable for
    OCR software. However, this significantly extends execution time of
    image creation.
    """

    def __init__(self, source, font,
                 size=(200, 80), margin=(20, 20),
                 **kwargs):
        r"""
        Captcha object init.

        Captcha object requires at least a text source (a string or a
        callable object returning a string) and a path to a TTF file. Both
        are used in generating text in returned CAPTCHA image with a given
        font. Callable object allow for creating variable CAPTCHAs without
        redeclaring Captcha instance, e.g. a randomized stream of characters

        :param source:
            String or a callable object returning a string.
        :param font:
            Valid path (relative or absolute) to a TTF file.
        :param size:
            A pair with CAPTCHA size (width, height)
            in pixels.
        :param margin:
            A pair with CAPTCHA x and y margins in pixels
            Note that generated text may slightly overlap
            given margins, you should treat them only as
            an estimate.
        :param \**kwargs:
            See below

        :Keyword Arguments:
            * *format* (``string``) --
              Image format acceptable by Image class from PIL package.
            * *resample* (``int``) --
              Resampling filter. Allowed: Image.NEAREST, Image.BILINEAR and
              Image.BICUBIC. Default: Image.BICUBIC.
            * *noise* (``float``) --
              Parameter from range [0,1] used in creating noise effect in
              CAPTCHA image. If not larger than 1/255, no noise if generated.
              It is advised to not use this option if you want to focus on
              efficiency, since generating noise can significantly extend
              image creation time. Default: 0.
        """
        self.source = source
        self.size = size
        self.margin = margin
        self.font = font

        self.format = kwargs.get('format', 'PNG')
        self.resample = kwargs.get('resample', Image.BICUBIC)
        self.noise = abs(kwargs.get('noise', 0.))

    @property
    def image(self):
        r"""
        Tuple with a CAPTCHA text and a Image object.

        Images are generated on the fly, using given text source, TTF font and
        other parameters passable through __init__. All letters in used text
        are morphed. Also a line is morphed and passed onto CAPTCHA text.
        Additionally, if self.noise > 1/255, a "snowy" image is merged with
        CAPTCHA image with a 50/50 ratio.
        Property returns a pair containing a string with text in returned
        image and image itself.

        :returns: ``tuple`` (CAPTCHA text, Image object)
        """
        text = self.text
        width, height = self.font.getsize(text)
        margin_x = round(self.margin_x * width / self.width)
        margin_y = round(self.margin_y * height / self.height)

        image = Image.new('RGB',
                          (width + 2 * margin_x, height + 2 * margin_y),
                          (255, 255, 255))

        # Text
        self.__write_text(image, text, pos=(margin_x, margin_y))

        # Line
        self.__draw_line(image)

        # White noise
        noise = self.__white_noise(image.size)
        if noise is not None:
            image = Image.blend(image, noise, 0.5)

        # Resize
        image = image.resize(self.size, resample=self.resample)

        return text, image

    @property
    def bytes(self):
        r"""
        Tuple with a CAPTCHA text and a BytesIO object.

        Property calls self.image and saves image contents in a BytesIO
        instance, returning CAPTCHA text and BytesIO as a tuple.
        See: image.

        :returns: ``tuple`` (CAPTCHA text, BytesIO object)
        """
        text, image = self.image
        _bytes = BytesIO()
        image.save(_bytes, format=self.format)
        _bytes.seek(0)
        return text, _bytes

    def write(self, file):
        r"""
        Save CAPTCHA image in given filepath.

        Property calls self.image and saves image contents in a file,
        returning CAPTCHA text and filepath as a tuple.
        See: image.

        :param file:
            Path to file, where CAPTCHA image will be saved.
        :returns: ``tuple`` (CAPTCHA text, filepath)
        """
        text, image = self.image
        image.save(file, format=self.format)
        return text, file

    @property
    def source(self):
        """Text source, either a string or a callable object."""
        return self.__source

    @source.setter
    def source(self, source):
        if not (isinstance(source, str) or callable(source)):
            raise CaptchaError("source has to be either a string or be callable")
        self.__source = source

    @property
    def text(self):
        """Text received from self.source."""
        if isinstance(self.source, str):
            return self.source
        return self.source()

    @property
    def size(self):
        """CAPTCHA image size."""
        return self.__size

    @size.setter
    @_with_pair_validator
    def size(self, size):
        self.__size = (int(size[0]), int(size[1]))

    @property
    def width(self):
        """CAPTCHA image width."""
        return self.size[0]

    @property
    def height(self):
        """CAPTCHA image height."""
        return self.size[1]

    @property
    def margin(self):
        """CAPTCHA image estimated margin."""
        return self.__margin

    @margin.setter
    @_with_pair_validator
    def margin(self, margin):
        if 2 * margin[1] > self.height:
            raise CaptchaError("Margin y cannot be larger than half of image height.")
        self.__margin = (int(margin[0]), int(margin[1]))

    @property
    def margin_x(self):
        """CAPTCHA image estimated x margin."""
        return self.__margin[0]

    @property
    def margin_y(self):
        """CAPTCHA image estimated y margin."""
        return self.__margin[1]

    @property
    def font(self):
        """ImageFont object from PIL package."""
        return self.__font

    @font.setter
    @_with_file_validator
    def font(self, font):
        if isinstance(font, ImageFont.ImageFont):
            self.__font = font
        else:
            fontsize = self.height - 2 * self.margin_y
            self.__font = ImageFont.truetype(font, fontsize)

    @property
    def noise(self):
        """Noise parameter from [0,1]."""
        return self.__noise

    @noise.setter
    def noise(self, noise):
        if noise < 0. or noise > 1.:
            raise CaptchaError("only acceptable noise amplitude from range [0:1]")
        self.__noise = noise

    def __write_text(self, image, text, pos):
        """Write morphed text in Image object."""
        offset = 0
        x_pos, y_pos = pos

        for char in text:
            # Write letter
            c_size = self.font.getsize(char)
            c_image = Image.new('RGBA', c_size, (0, 0, 0, 0))
            c_draw = ImageDraw.Draw(c_image)
            c_draw.text((0, 0), char, font=self.font, fill=(0, 0, 0, 255))

            # Transform
            c_image = self.__rnd_letter_transform(c_image)

            # Paste onto image
            image.paste(c_image, (x_pos + offset, y_pos), c_image)
            offset += c_size[0]

    def __draw_line(self, image):
        """Draw morphed line in Image object."""
        width, height = image.size
        width *= 5
        height *= 5

        l_image = Image.new('RGBA', (width, height), (0, 0, 0, 0))
        l_draw = ImageDraw.Draw(l_image)

        self.draw_line_in_image(l_draw, width, height)
        self.draw_line_in_image(l_draw, width, height)

        # Transform
        l_image = self.__rnd_line_transform(l_image)
        l_image = l_image.resize(image.size, resample=self.resample)

        # Paste onto image
        image.paste(l_image, (0, 0), l_image)

    def __white_noise(self, size):
        """Generate white noise and merge it with given Image object."""
        if self.noise > 0.003921569:  # 1./255.
            width, height = size

            pixel = (lambda noise: round(255 * random.uniform(1 - noise, 1)))

            n_image = Image.new('RGB', size, (0, 0, 0, 0))
            rnd_grid = map(lambda _: tuple([pixel(self.noise)]) * 3,
                           [0] * width * height)
            n_image.putdata(list(rnd_grid))
            return n_image

        return None

    def __rnd_letter_transform(self, image):
        """Randomly morph a single character."""
        width, height = image.size

        delta_x = width * random.uniform(0.1, 0.3)
        delta_y = height * random.uniform(0.1, 0.3)

        _x1, _y1 = self.__class__.rnd_point_disposition(delta_x, delta_y)
        _x2, _y2 = self.__class__.rnd_point_disposition(delta_x, delta_y)

        width += abs(_x1) + abs(_x2)
        height += abs(_x1) + abs(_x2)

        quad = self.__class__.quad_points((width, height), (_x1, _y1), (_x2, _y2))

        return image.transform(image.size, Image.QUAD,
                               data=quad, resample=self.resample)

    def __rnd_line_transform(self, image):
        """Randomly morph Image object with drawn line."""
        width, height = image.size

        delta_x = width * random.uniform(0.2, 0.5)
        delta_y = height * random.uniform(0.2, 0.5)

        _x1, _y1 = [abs(z) for z in self.__class__.rnd_point_disposition(delta_x, delta_y)]
        _x2, _y2 = [abs(z) for z in self.__class__.rnd_point_disposition(delta_x, delta_y)]

        quad = self.__class__.quad_points((width, height), (_x1, _y1), (_x2, _y2))

        return image.transform(image.size, Image.QUAD,
                               data=quad, resample=self.resample)

    @staticmethod
    def rnd_point_disposition(delta_x, delta_y):
        """Return random disposition point."""
        new_x = int(random.uniform(-delta_x, delta_x))
        new_y = int(random.uniform(-delta_y, delta_y))
        return new_x, new_y

    @staticmethod
    def quad_points(size, disp1, disp2):
        """Return points for QUAD transformation."""
        width, height = size
        _x1, _y1 = disp1
        _x2, _y2 = disp2

        return (
            _x1, -_y1,
            -_x1, height + _y2,
            width + _x2, height - _y2,
            width - _x2, _y1
        )

    @staticmethod
    def draw_line_in_image(image, image_width, image_height):
        """Return points for QUAD transformation."""
        _x1 = int(image_width * random.uniform(0, 0.1))
        _y1 = int(image_height * random.uniform(0, 1))
        _x2 = int(image_width * random.uniform(0.9, 1))
        _y2 = int(image_height * random.uniform(0, 1))

        # Line width modifier was chosen as an educated guess
        # based on default image area.
        l_width = round((image_width * image_height) ** 0.4 * 2.284e-2)

        # Draw
        image.line(((_x1, _y1), (_x2, _y2)), fill=(0, 0, 0, 255), width=l_width)


def _with_pair_validator(func):
    @wraps(func)
    def wrapper(inst, pair):
        if not (hasattr(pair, '__len__') and hasattr(pair, '__getitem__')):
            raise CaptchaError("Sequence not provided")
        if len(pair) != 2:
            raise CaptchaError("Sequence has to have exactly 2 elements")
        return func(inst, pair)

    return wrapper


def _with_file_validator(func):
    @wraps(func)
    def wrapper(inst, file):
        if not isinstance(file, ImageFont.ImageFont):
            if not os.path.exists(file):
                raise CaptchaError("%s doesn't exist" % (file,))
            if not os.path.isfile(file):
                raise CaptchaError("%s is not a file" % (file,))
        return func(inst, file)

    return wrapper
