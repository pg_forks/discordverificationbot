from discord.ext import commands

bot = commands.Bot(command_prefix='!')


@bot.command()  # Не передаём аргумент pass_context, так как он был нужен в старых версиях.
async def hello(ctx):  # Создаём функцию и передаём аргумент ctx.
    author = ctx.message.author  # Объявляем переменную author и записываем туда информацию об авторе.
    await ctx.send(
        f'Hello, {author.mention}!')  # Выводим сообщение с упоминанием автора, обращаясь к переменной author.


bot.run(
    'ODA1NDYwODA4NDgxODk4NTM2.YBbN1Q.oBYZo_MbrADpnaONLWDBYUtxBMw')  # Обращаемся к словарю settings с ключом token, для получения токена
